package id.co.danwinciptaniaga.jessys.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.haulmont.addon.sdbmt.core.TenantId;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseUuidEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.TenantEntity;
import com.haulmont.cuba.core.entity.Updatable;

@NamePattern("%s|name")
@Table(name = "JESSYS_SUPPLIER")
@Entity(name = "jessys_Supplier")
public class Supplier extends BaseUuidEntity
    implements Updatable, Creatable, TenantEntity {
  private static final long serialVersionUID = 6826065752055347048L;

  @Column(name = "UPDATE_TS")
  protected Date updateTs;

  @Column(name = "UPDATED_BY", length = 50)
  protected String updatedBy;

  @Column(name = "CREATE_TS")
  protected Date createTs;

  @Column(name = "CREATED_BY", length = 50)
  protected String createdBy;

  @NotNull
  @Column(name = "NAME", nullable = false, unique = true)
  protected String name;

  @NotNull
  @Column(name = "ACTIVE", nullable = false, columnDefinition = "boolean not null default true")
  protected Boolean active = true;

  @TenantId
  @Column(name = "TENANT_ID")
  protected String tenantId;

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getCreatedBy() {
    return createdBy;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public Date getCreateTs() {
    return createTs;
  }

  @Override
  public void setCreateTs(Date createTs) {
    this.createTs = createTs;
  }

  @Override
  public String getUpdatedBy() {
    return updatedBy;
  }

  @Override
  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Override
  public Date getUpdateTs() {
    return updateTs;
  }

  @Override
  public void setUpdateTs(Date updateTs) {
    this.updateTs = updateTs;
  }

  public String getTenantId() {
    return tenantId;
  }

  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }
}