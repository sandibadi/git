package id.co.danwinciptaniaga.jessys.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;

public enum ExistingOrNew implements EnumClass<String> {

  NEW("N"),
  EXISTING("E");

  private String id;

  ExistingOrNew(String value) {
    this.id = value;
  }

  public String getId() {
    return id;
  }

  @Nullable
  public static ExistingOrNew fromId(String id) {
    for (ExistingOrNew at : ExistingOrNew.values()) {
      if (at.getId().equals(id)) {
        return at;
      }
    }
    return null;
  }
}