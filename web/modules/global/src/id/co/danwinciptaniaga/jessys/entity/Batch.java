package id.co.danwinciptaniaga.jessys.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.haulmont.addon.sdbmt.core.TenantId;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.BaseUuidEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.TenantEntity;
import com.haulmont.cuba.core.entity.Updatable;

@NamePattern("%s|batchNo")
@Table(name = "JESSYS_BATCH")
@Entity(name = "jessys_Batch")
public class Batch extends BaseUuidEntity implements Updatable, Creatable, TenantEntity {
  private static final long serialVersionUID = -5633868570559553705L;

  @Column(name = "UPDATE_TS")
  protected Date updateTs;

  @Column(name = "UPDATED_BY", length = 50)
  protected String updatedBy;

  @Column(name = "CREATE_TS")
  protected Date createTs;

  @Column(name = "CREATED_BY", length = 50)
  protected String createdBy;

  @Positive
  @NotNull
  @Column(name = "BATCH_NO", nullable = false, unique = true)
  protected Integer batchNo;

  @Temporal(TemporalType.DATE)
  @Column(name = "START_DATE")
  protected Date startDate;

  @Temporal(TemporalType.DATE)
  @Column(name = "END_DATE")
  protected Date endDate;

  @NotNull
  @Column(name = "ACTIVE", nullable = false)
  protected Boolean active = true;

  @TenantId
  @Column(name = "TENANT_ID")
  protected String tenantId;

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Integer getBatchNo() {
    return batchNo;
  }

  public void setBatchNo(Integer batchNo) {
    this.batchNo = batchNo;
  }

  @Override
  public String getCreatedBy() {
    return createdBy;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public Date getCreateTs() {
    return createTs;
  }

  @Override
  public void setCreateTs(Date createTs) {
    this.createTs = createTs;
  }

  @Override
  public String getUpdatedBy() {
    return updatedBy;
  }

  @Override
  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Override
  public Date getUpdateTs() {
    return updateTs;
  }

  @Override
  public void setUpdateTs(Date updateTs) {
    this.updateTs = updateTs;
  }

  public String getTenantId() {
    return tenantId;
  }

  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }
}