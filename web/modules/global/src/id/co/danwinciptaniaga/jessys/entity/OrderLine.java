package id.co.danwinciptaniaga.jessys.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

import com.haulmont.addon.sdbmt.core.TenantId;
import com.haulmont.cuba.core.entity.BaseUuidEntity;
import com.haulmont.cuba.core.entity.Creatable;
import com.haulmont.cuba.core.entity.TenantEntity;
import com.haulmont.cuba.core.entity.Updatable;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;

@Table(name = "JESSYS_ORDER_LINE")
@Entity(name = "jessys_OrderLine")
public class OrderLine extends BaseUuidEntity
    implements Updatable, Creatable, TenantEntity {
  private static final long serialVersionUID = 3879307984553668016L;

  @Column(name = "UPDATE_TS")
  protected Date updateTs;

  @Column(name = "UPDATED_BY", length = 50)
  protected String updatedBy;

  @Column(name = "CREATE_TS")
  protected Date createTs;

  @Column(name = "CREATED_BY", length = 50)
  protected String createdBy;

  @Positive
  @NotNull
  @Column(name = "LINE", nullable = false)
  protected Integer line;

  @OnDeleteInverse(DeletePolicy.CASCADE)
  @NotNull
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "ORDER_ID")
  protected Order order;

  @Lookup(type = LookupType.DROPDOWN, actions = {})
  @NotNull
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "PRODUCT_ID")
  protected Product product;

  @PositiveOrZero
  @NotNull
  @Column(name = "PRICE", nullable = false, precision = 19, scale = 0)
  protected BigDecimal unitPrice;

  @Positive
  @NotNull
  @Column(name = "QUANTITY", nullable = false)
  protected Integer quantity;

  @NotNull
  @Column(name = "LINE_TOTAL", nullable = false, precision = 19, scale = 0)
  protected BigDecimal lineTotal;

  @TenantId
  @Column(name = "TENANT_ID")
  protected String tenantId;

  public Integer getLine() {
    return line;
  }

  public void setLine(Integer line) {
    this.line = line;
  }

  public BigDecimal getLineTotal() {
    return lineTotal;
  }

  public void setLineTotal(BigDecimal lineTotal) {
    this.lineTotal = lineTotal;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public BigDecimal getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(BigDecimal unitPrice) {
    this.unitPrice = unitPrice;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  @Override
  public String getCreatedBy() {
    return createdBy;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public Date getCreateTs() {
    return createTs;
  }

  @Override
  public void setCreateTs(Date createTs) {
    this.createTs = createTs;
  }

  @Override
  public String getUpdatedBy() {
    return updatedBy;
  }

  @Override
  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Override
  public Date getUpdateTs() {
    return updateTs;
  }

  @Override
  public void setUpdateTs(Date updateTs) {
    this.updateTs = updateTs;
  }

  public String getTenantId() {
    return tenantId;
  }

  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }
}