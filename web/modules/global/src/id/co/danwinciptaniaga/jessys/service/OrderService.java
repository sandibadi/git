package id.co.danwinciptaniaga.jessys.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Set;

import id.co.danwinciptaniaga.jessys.entity.Order;
import id.co.danwinciptaniaga.jessys.entity.OrderLine;

public interface OrderService {
  String NAME = "jessys_OrderService";

  BigDecimal calculateOrderLineTotal(OrderLine orderLine);

  BigDecimal calculateOrderLinesTotal(Collection<OrderLine> orderLines);

  BigDecimal calculateOrderTotal(Order order, Collection<OrderLine> orderLines);

  void bookOrder(Order order);

  void unbookOrder(Order order);

  byte[] print(Set<Order> orders) throws Exception;
}