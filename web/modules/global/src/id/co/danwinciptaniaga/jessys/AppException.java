package id.co.danwinciptaniaga.jessys;

import com.haulmont.cuba.core.global.SupportedByClient;

@SupportedByClient
public class AppException extends Exception {

  public AppException(String message, Throwable throwable) {
    super(message, throwable);
  }
}
