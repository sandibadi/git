package id.co.danwinciptaniaga.jessys.web.screens.product;

import com.haulmont.cuba.gui.screen.*;
import id.co.danwinciptaniaga.jessys.entity.Product;

@UiController("jessys_Product.edit")
@UiDescriptor("product-edit.xml")
@EditedEntityContainer("productDc")
@LoadDataBeforeShow
public class ProductEdit extends StandardEditor<Product> {
}