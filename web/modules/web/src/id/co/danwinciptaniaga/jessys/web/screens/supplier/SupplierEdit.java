package id.co.danwinciptaniaga.jessys.web.screens.supplier;

import com.haulmont.cuba.gui.screen.*;
import id.co.danwinciptaniaga.jessys.entity.Supplier;

@UiController("jessys_Supplier.edit")
@UiDescriptor("supplier-edit.xml")
@EditedEntityContainer("supplierDc")
@LoadDataBeforeShow
public class SupplierEdit extends StandardEditor<Supplier> {
}