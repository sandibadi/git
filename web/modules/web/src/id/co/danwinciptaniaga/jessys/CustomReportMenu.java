package id.co.danwinciptaniaga.jessys;

import javax.inject.Inject;
import org.springframework.stereotype.Component;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.reports.entity.Report;

@Component(CustomReportMenu.NAME)
public class CustomReportMenu {
  public static final String NAME = "jessys_NewReportMenu";

  public static final String INVOICE_REPORT_NAME = "Invoice";
  public static final String REKAP_ORDER_REPORT_NAME = "Rekap Order";
  public static final String SALES_ORDER_REPORT_NAME = "Laporan Sales Order";

  @Inject
  private UserSessionSource userSessionSource;
  @Inject
  private DataManager dataManager;
  @Inject
  private CustomReportGuiManager customReportGuiManager;

  public void runInvoiceReport() {
    Report invoiceReport = customReportGuiManager.getAvailableReportByName(null,
        userSessionSource.getUserSession().getUser(), null, INVOICE_REPORT_NAME);
    if (invoiceReport != null) {
      Report r = dataManager.load(Report.class).view("report.edit").id(invoiceReport.getId()).one();
      customReportGuiManager.runReportNewTab(r, null);
    }
  }

  public void runRekapOrderReport() {
    Report rekapOrderReport = customReportGuiManager.getAvailableReportByName(null,
        userSessionSource.getUserSession().getUser(), null, REKAP_ORDER_REPORT_NAME);
    if (rekapOrderReport != null) {
      Report r = dataManager.load(Report.class).view("report.edit").id(
          rekapOrderReport.getId()).one();
      customReportGuiManager.runReportNewTab(r, null);
    }
  }

  public void runSalesOrderReport() {
    Report salesOrderReport = customReportGuiManager.getAvailableReportByName(null,
        userSessionSource.getUserSession().getUser(), null, SALES_ORDER_REPORT_NAME);
    if (salesOrderReport != null) {
      Report r = dataManager.load(Report.class).view("report.edit").id(
          salesOrderReport.getId()).one();
      customReportGuiManager.runReportNewTab(r, null);
    }

  }
}