package id.co.danwinciptaniaga.jessys.web.screens.order;

import java.util.Optional;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.haulmont.cuba.core.global.MetadataTools;
import com.haulmont.cuba.gui.Notifications;
import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.DataGrid;
import com.haulmont.cuba.gui.components.actions.BaseAction;
import com.haulmont.cuba.gui.export.ByteArrayDataProvider;
import com.haulmont.cuba.gui.export.ExportDisplay;
import com.haulmont.cuba.gui.export.ExportFormat;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.screen.LoadDataBeforeShow;
import com.haulmont.cuba.gui.screen.LookupComponent;
import com.haulmont.cuba.gui.screen.MessageBundle;
import com.haulmont.cuba.gui.screen.StandardLookup;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;

import id.co.danwinciptaniaga.jessys.OrderHelper;
import id.co.danwinciptaniaga.jessys.UIHelper;
import id.co.danwinciptaniaga.jessys.entity.Order;
import id.co.danwinciptaniaga.jessys.service.OrderService;

@UiController("jessys_Order.browse")
@UiDescriptor("order-browse.xml")
@LookupComponent("ordersTable")
@LoadDataBeforeShow
public class OrderBrowse extends StandardLookup<Order> {
  private static Logger logger = LoggerFactory.getLogger(OrderBrowse.class);

  @Inject
  private MessageBundle messageBundle;

  @Inject
  private OrderService orderService;
  @Inject
  private Notifications notifications;
  @Inject
  private ExportDisplay exportDisplay;

  @Inject
  private DataGrid<Order> ordersTable;
  @Inject
  private CollectionLoader<Order> ordersDl;
  @Inject
  private Button bookBtn;
  @Inject
  private Button unbookBtn;
  @Inject
  private Button printBtn;
  @Inject
  private OrderHelper orderHelper;
  @Inject
  private MetadataTools metadataTools;

  @Subscribe
  public void onInit(InitEvent event) {
    Action bookOrderAction = new BaseAction("bookOrder")
        .withCaption(messageBundle.getMessage("btnBook"))
        .withPrimary(true)
        .withHandler(this::onBookButtonClick);

    bookBtn.setAction(bookOrderAction);
    bookBtn.setEnabled(false);

    Action unbookOrderAction = new BaseAction("unbookOrder")
        .withCaption(messageBundle.getMessage("btnUnbook"))
        .withPrimary(true)
        .withHandler(this::onUnbookButtonClick);

    unbookBtn.setAction(unbookOrderAction);
    unbookBtn.setEnabled(false);

    Action printAction = new BaseAction("printInvoice")
        .withCaption(messageBundle.getMessage("printInvoice"))
        .withPrimary(true)
        .withHandler(this::onPrintButtonClick);

    printBtn.setAction(printAction);
    printBtn.setEnabled(false);
  }

  private void onPrintButtonClick(Action.ActionPerformedEvent actionPerformedEvent) {
    try {
      byte[] result = orderService.print(ordersTable.getSelected());

      exportDisplay.show(new ByteArrayDataProvider(result), "invoice.pdf", ExportFormat.PDF);
    } catch (Exception e) {
      logger.error("Failed to print invoice", e);
      UIHelper.showNotification(notifications,
          messageBundle.formatMessage("notification.order.printFail", e),
          Notifications.NotificationType.ERROR);
    } finally {

    }
  }

  private void onBookButtonClick(Action.ActionPerformedEvent actionPerformedEvent) {
    Order o = ordersTable.getSingleSelected();
    // book Order
    try {
      orderService.bookOrder(o);
      ordersDl.getContainer().getItems().stream().forEach(e -> {
        ordersDl.getDataContext().evict(e);
      });
      ordersDl.load();
      o = ordersDl.getContainer().getItem(o.getId());
      ordersTable.setSelected(o);
      UIHelper.showNotification(notifications,
          String.format(messageBundle.getMessage("notification.order.bookedSuccess"),
              metadataTools.getInstanceName(o)),
          Notifications.NotificationType.HUMANIZED);
    } catch (Exception e) {
      UIHelper.showNotification(notifications,
          String.format(messageBundle.getMessage("notification.order.bookFail"),
              metadataTools.getInstanceName(o),
              e.getMessage()),
          Notifications.NotificationType.ERROR);
    }
  }

  private void onUnbookButtonClick(Action.ActionPerformedEvent actionPerformedEvent) {
    Order o = ordersTable.getSingleSelected();
    // unbook Order
    try {
      orderService.unbookOrder(o);
      ordersDl.getContainer().getItems().stream().forEach(e -> {
        ordersDl.getDataContext().evict(e);
      });
      ordersDl.load();
      // refresh selected order
      o = ordersDl.getContainer().getItem(o.getId());
      ordersTable.setSelected(o);
      UIHelper.showNotification(notifications,
          String.format(messageBundle.getMessage("notification.order.unbookSuccess"),
              metadataTools.getInstanceName(o)),
          Notifications.NotificationType.HUMANIZED);
    } catch (Exception e) {
      UIHelper.showNotification(notifications,
          String.format(messageBundle.getMessage("notification.order.unbookFail"),
              metadataTools.getInstanceName(o),
              e.getMessage()),
          Notifications.NotificationType.ERROR);
    }
  }

  @Subscribe("ordersTable")
  public void onOrdersTableSelection(DataGrid.SelectionEvent<Order> event) {
    logger.debug("Selected {} Order(s)", CollectionUtils.size(ordersTable.getSelected()));
    if (CollectionUtils.size(ordersTable.getSelected()) == 0) {
      // kalau tidak ada yang dipilih
      printBtn.setEnabled(false);
      bookBtn.setEnabled(false);
      unbookBtn.setEnabled(false);
    } else if (CollectionUtils.size(ordersTable.getSelected()) == 1) {
      Optional<Order> oOrder = ordersTable.getSelected().stream().findFirst();
      if (oOrder.isPresent() && Boolean.valueOf(oOrder.get().getBooked())) {
        logger.debug("selected Order {} is booked", oOrder.get());
        // kalau order sudah booked, tidak bisa dihapus lagi
        event.getSource().getAction("remove").setEnabled(false);
        bookBtn.setEnabled(false);
        unbookBtn.setEnabled(true);
      } else {
        // kalau order belum booked, masih bisa dihapus lagi
        logger.debug("selected Order {} is NOT booked", oOrder.get());
        event.getSource().getAction("remove").setEnabled(true);
        bookBtn.setEnabled(true);
        unbookBtn.setEnabled(false);
      }
      // bisa edit multiple row
      event.getSource().getAction("edit").setEnabled(true);
      // bisa print 1
      printBtn.setEnabled(true);
    } else {
      logger.debug("selected Orders are booked");
      // tidak bisa edit multiple row
      event.getSource().getAction("edit").setEnabled(false);
      // bisa print multiple row
      bookBtn.setEnabled(false);
      unbookBtn.setEnabled(false);
      printBtn.setEnabled(true);
    }
  }
}