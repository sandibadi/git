package id.co.danwinciptaniaga.jessys.web.screens.product;

import com.haulmont.cuba.gui.screen.*;
import id.co.danwinciptaniaga.jessys.entity.Product;

@UiController("jessys_Product.browse")
@UiDescriptor("product-browse.xml")
@LookupComponent("productsTable")
@LoadDataBeforeShow
public class ProductBrowse extends StandardLookup<Product> {
}