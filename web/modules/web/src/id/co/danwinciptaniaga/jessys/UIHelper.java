package id.co.danwinciptaniaga.jessys;

import com.haulmont.cuba.gui.Notifications;

public class UIHelper {
  public static void showNotification(Notifications notifications, String caption,
      Notifications.NotificationType type) {
    notifications.create()
        .withCaption(caption)
        .withType(type)
        .show();
  }
}
