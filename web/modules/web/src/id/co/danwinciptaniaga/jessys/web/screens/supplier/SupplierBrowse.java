package id.co.danwinciptaniaga.jessys.web.screens.supplier;

import com.haulmont.cuba.gui.screen.*;
import id.co.danwinciptaniaga.jessys.entity.Supplier;

@UiController("jessys_Supplier.browse")
@UiDescriptor("supplier-browse.xml")
@LookupComponent("suppliersTable")
@LoadDataBeforeShow
public class SupplierBrowse extends StandardLookup<Supplier> {
}