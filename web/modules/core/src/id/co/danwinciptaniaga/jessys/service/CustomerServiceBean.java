package id.co.danwinciptaniaga.jessys.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haulmont.cuba.core.TransactionalDataManager;

@Service(CustomerService.NAME)
public class CustomerServiceBean implements CustomerService {
  @Inject
  private TransactionalDataManager transactionalDataManager;

  @Override
  @Transactional
  public long getNextCustCode() {
    Long result = transactionalDataManager
        .loadValue("select max(e.code) from jessys_Customer e", Long.class)
        .one();
    return result == null ? 1 : result + 1;
  }
}