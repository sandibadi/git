package id.co.danwinciptaniaga.jessys;

import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleHtmlReportConfiguration;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;

import static com.google.common.base.Preconditions.*;

public class JasperUtil {
  private static Logger logger = LoggerFactory.getLogger(JasperUtil.class);

  public static final String MODE_PDF = "pdf";
  public static final String MODE_XLS = "xls";
  public static final String MODE_HTML = "html";

  public static void printReports(InputStream templateIs, List<Map<String, Object>> parametersList,
      String mode, OutputStream outputStream) throws Exception {
    JasperReport jr = JasperCompileManager.compileReport(templateIs);
    printReports(jr, parametersList, mode, outputStream);
  }

  public static void printReports(String path, List<Map<String, Object>> parametersList,
      String mode, OutputStream outputStream) throws Exception {
    JasperReport jr = JasperCompileManager.compileReport(path);
    printReports(jr, parametersList, mode, outputStream);
  }

  public static void printReports(JasperReport jr, List<Map<String, Object>> parametersList,
      String mode, OutputStream outputStream) throws Exception {
    logger.debug("-> start print {} report(s)<-", CollectionUtils.size(parametersList));
    checkArgument(CollectionUtils.size(parametersList) > 0,
        "parametersList must contain something");
    try {
      List<JasperPrint> jasperPrints = new ArrayList<>();
      for (Map<String, Object> parameters : parametersList) {
        if (!mode.equals(MODE_PDF)) {
          parameters.put(JRParameter.IS_IGNORE_PAGINATION, true);
        }

        JasperPrint jp = JasperFillManager.fillReport(jr, parameters);
        jasperPrints.add(jp);
      }
      BufferedOutputStream output = new BufferedOutputStream(outputStream);
      if (mode.equals(MODE_XLS)) {
        logger.debug("-> print XLS mode <-");

        JRXlsExporter exporter = new JRXlsExporter();
        exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrints));
        SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
        configuration.setDetectCellType(true);
        configuration.setAutoFitPageHeight(true);
        configuration.setImageBorderFixEnabled(true);
        exporter.setConfiguration(configuration);
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(output));
        exporter.exportReport();
      } else if (mode.equals(MODE_PDF)) {
        logger.debug("-> print PDF mode <-");

        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrints));
        SimplePdfReportConfiguration configuration = new SimplePdfReportConfiguration();
        exporter.setConfiguration(configuration);
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(output));
        exporter.exportReport();
      } else if (mode.equals(MODE_HTML)) {
        logger.debug("-> print HTML mode <-");

        HtmlExporter exporter = new HtmlExporter();
        exporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrints));
        exporter.setExporterOutput(new SimpleHtmlExporterOutput(output));
        SimpleHtmlReportConfiguration htmlConfig = new SimpleHtmlReportConfiguration();
        htmlConfig.setZoomRatio(1.5f);
        exporter.setConfiguration(htmlConfig);
        exporter.exportReport();
      }
      output.flush();
      logger.debug("-> print done <-");
    } catch (Exception e) {
      logger.error("Problem executing Jasper", e);
      throw e;
    }
  }
}
