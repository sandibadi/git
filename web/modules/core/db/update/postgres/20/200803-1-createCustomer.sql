create table JESSYS_CUSTOMER (
    ID uuid,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    --
    CODE bigint not null,
    NAME varchar(255),
    ADDRESS varchar(255) not null,
    PHONE varchar(15),
    --
    primary key (ID)
);