alter table JESSYS_SUPPLIER add column ACTIVE boolean not null default true ^
update JESSYS_SUPPLIER set ACTIVE = false where ACTIVE is null ;
alter table JESSYS_SUPPLIER alter column ACTIVE set not null ;
