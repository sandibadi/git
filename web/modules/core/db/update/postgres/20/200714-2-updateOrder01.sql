alter table JESSYS_ORDER add constraint FK_JESSYS_ORDER_ON_BATCH foreign key (BATCH_ID) references JESSYS_BATCH(ID);
create index IDX_JESSYS_ORDER_ON_BATCH on JESSYS_ORDER (BATCH_ID);
