create table JESSYS_PRODUCT (
    ID uuid,
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    --
    CODE varchar(20) not null,
    NAME varchar(255) not null,
    SUPPLIER_ID uuid not null,
    PURCHASE_PRICE decimal(19) not null,
    SELLING_PRICE decimal(19),
    --
    primary key (ID)
);