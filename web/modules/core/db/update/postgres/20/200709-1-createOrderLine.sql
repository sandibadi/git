create table JESSYS_ORDER_LINE (
    ID uuid,
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    --
    ORDER_ID uuid not null,
    PRODUCT_ID uuid not null,
    PRICE decimal(19) not null,
    QUANTITY integer not null,
    LINE_TOTAL decimal(19) not null,
    --
    primary key (ID)
);